﻿using UnityEngine;
using System.Collections;

public class Tap_Control : MonoBehaviour
{
    public bool mouse = false;
    // Use this for initialization
    void Start ()
    {
	
    }
	
    // Update is called once per frame
    void Update ()
    {
        if (mouse)
        {
            Click ();
        }
        else
        {
            Tap ();
        }
    }

    //:::::::::::::::::::::::::::::::
    // Tap
    //:::::::::::::::::::::::::::::::
    void Tap ()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            // Handle finger movements based on touch phase.
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    Vector2 pos = new Vector2(Camera.main.ScreenToWorldPoint(touch.position).x, Camera.main.ScreenToWorldPoint(touch.position).y);
                    Collider2D other = Physics2D.OverlapPoint(pos);

                    if (other != null && other.tag == "Box")
                    {
                        //print("Touch Change " + other.name);
                        other.SendMessage("Change_State");
                    }
                    else
                        print("Touch Error !");
                    break;
            }
        }
    }

    void Click ()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 pos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            Collider2D other = Physics2D.OverlapPoint(pos);

            if (other != null && other.tag == "Box")
            {
                //print("Click Change " + other.name);
                other.SendMessage("Change_State");
            }
        }
    }
}
