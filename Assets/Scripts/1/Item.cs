﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour
{

    public bool state;

    //:::::::::::::::::::::::::::::::
    // OnTriggerEnter2D
    //:::::::::::::::::::::::::::::::
    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "Box")
        {
            if (state)
                other.gameObject.SendMessage("Collision_A", gameObject);
            else
                other.gameObject.SendMessage("Collision_B", gameObject);
        }

    }
}
