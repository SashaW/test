﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Box : MonoBehaviour
{
    public bool state = true;
    public Sprite state1, state2;
    public Color c1, c2;
    public Text score;

    int points;

    //:::::::::::::::::::::::::::::::
    // Update
    //:::::::::::::::::::::::::::::::
    void Update ()
    {
        if (state)
        {
            GetComponent<SpriteRenderer>().sprite = state1;
            GetComponent<SpriteRenderer>().color = c1;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = state2;
            GetComponent<SpriteRenderer>().color = c2;
        }
    }

    //:::::::::::::::::::::::::::::::
    // Change_State
    //:::::::::::::::::::::::::::::::
    void Change_State ()
    {
        state = !state;
    }

    //:::::::::::::::::::::::::::::::
    // OnTriggerEnter2D
    //:::::::::::::::::::::::::::::::
    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "ItemA")
        {
            Collision_A(other.gameObject);
        }
        else if (other.gameObject.tag == "ItemB")
        {
            Collision_B(other.gameObject);
        }
        else if (other.gameObject.tag == "ItemFinish")
        {
            print("Finish Level x");
            GameObject.FindGameObjectWithTag("GameController").SendMessage("Finish_Level");
            GameObject.FindGameObjectWithTag("Spawner").SendMessage("Add_To_ListA", other.gameObject);
        }

    }

    //:::::::::::::::::::::::::::::::
    // Collision_A
    //:::::::::::::::::::::::::::::::
    void Collision_A (GameObject item)
    {
        if (state)
        {
            points++;
            GameObject.FindGameObjectWithTag("GameController").SendMessage("addScore", 1);
        }
        else
            GameObject.FindGameObjectWithTag("GameController").SendMessage("Error");

        score.text = "" + points;

        GameObject.FindGameObjectWithTag("Spawner").SendMessage("Add_To_ListA", item);

        //Destroy(item, 0.2f);
    }

    //:::::::::::::::::::::::::::::::
    // Collision_B
    //:::::::::::::::::::::::::::::::
    void Collision_B (GameObject item)
    {
        if (!state)
            points++;
        else
            GameObject.FindGameObjectWithTag("GameController").SendMessage("Error");

        score.text = "" + points;

        GameObject.FindGameObjectWithTag("Spawner").SendMessage("Add_To_ListB", item);
        //Destroy(item, 0.2f);
    }



}
