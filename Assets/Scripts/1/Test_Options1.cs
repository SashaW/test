﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Test_Options1 : MonoBehaviour
{
    public bool options;
    public float unit = 0.1f;

    public Text uiSpeed;
    public Text uiRate;
    float speed = 2;
    float rate = 3;

    // Use this for initialization
    void Awake ()
    {
        string prefs = PlayerPrefs.GetString("Player02");

        if (prefs == "Player02")
        {
            print ("Load Prefs !");
            Load_Prefs();
            Set_All ();
        } else {
            print ("Save Prefs !");
            Save_Prefs();
            Set_All ();
            PlayerPrefs.SetString("Player02", "Player02");
        }
    }

    // Use this for initialization
    void Start ()
    {
	
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }

    //:::::::::::::::::::::::::::::::
    // ShowOptions
    //:::::::::::::::::::::::::::::::
    public void ShowOptions ()
    {
        Time.timeScale = 0;
        GameObject go = GameObject.FindGameObjectWithTag("options");

        for (int i = 0; i < go.transform.childCount; i++)
            go.transform.GetChild(i).gameObject.SetActive(true);

        Update_UI();
    }

    //:::::::::::::::::::::::::::::::
    // HideOptions
    //:::::::::::::::::::::::::::::::
    public void HideOptions ()
    {
        Time.timeScale = 1;
        GameObject go = GameObject.FindGameObjectWithTag("options");

        for (int i = 0; i < go.transform.childCount; i++)
            go.transform.GetChild(i).gameObject.SetActive(false);
    }

    //:::::::::::::::::::::::::::::::
    // Options
    //:::::::::::::::::::::::::::::::
    public void Options ()
    {
        print("Options " + options);

        if (options)
            HideOptions();
        else
            ShowOptions();

        options = !options;

    }

    //:::::::::::::::::::::::::::::::
    // Options 
    //:::::::::::::::::::::::::::::::

    public void _Rate_Down ()
    {
        rate -= unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Rate_Up ()
    {
        rate += unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Speed_Down ()
    {
        speed -= unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Speed_Up ()
    {
        speed += unit;
        Update_UI();
        Save_Prefs();
    }

    //:::::::::::::::::::::::::::::::
    // Update_UI
    //:::::::::::::::::::::::::::::::
    void Update_UI ()
    {
        uiSpeed.text = speed.ToString("0.0");
        uiRate.text = rate.ToString("0.0");

        Set_All();
    }

    //:::::::::::::::::::::::::::::::
    // Load and Save 
    //:::::::::::::::::::::::::::::::
    void Load_Prefs ()
    {
        speed = PlayerPrefs.GetFloat("speed");
        rate = PlayerPrefs.GetFloat("rate");
    }

    void Save_Prefs ()
    {
        PlayerPrefs.SetFloat("speed", speed);
        PlayerPrefs.SetFloat("rate", rate);
    }

    void Set_All ()
    {
        GameObject spawner = GameObject.FindWithTag("Spawner");

        spawner.SendMessage("Set_Speed", speed);
        spawner.SendMessage("Set_Rate", rate);
    }
}
