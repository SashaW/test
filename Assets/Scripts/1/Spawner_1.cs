﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner_1 : MonoBehaviour
{

    public Transform[] pointsSpawn;
    public GameObject[] items;
    public Transform container;

    public float speed;
    public float rate;

    public List<GameObject> listA = new List<GameObject>();
    public List<GameObject> listB = new List<GameObject>();

    private GameObject lastItem;
    //:::::::::::::::::::::::::::::::
    // Start
    //:::::::::::::::::::::::::::::::
    void StartGame ()
    {
        StartCoroutine("Init_Level");
        StartCoroutine("Init_Spawn");
    }

    //:::::::::::::::::::::::::::::::
    // FinishGame
    //:::::::::::::::::::::::::::::::
    void FinishGame ()
    {
        StopCoroutine("Init_Spawn");

        for (int i = 0; i < container.childCount; i++)
        {
            container.GetChild(i).GetComponent<CircleCollider2D>().enabled = false;
        }
    }

    //:::::::::::::::::::::::::::::::
    // Update
    //:::::::::::::::::::::::::::::::
    void Update ()
    {
        
    }

    //:::::::::::::::::::::::::::::::
    // Init_Spawn
    //:::::::::::::::::::::::::::::::
    IEnumerator Init_Spawn ()
    {       

        /*for (int i = 0; i < pointsSpawn.Length; i++)
        {
            int r = Random.Range(0, items.Length);

            GameObject go = Instantiate(items[r], pointsSpawn[i].position, Quaternion.identity) as GameObject;
            go.transform.SetParent(container);
            go.GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;

        }*/

        int p = Random.Range(0, pointsSpawn.Length);
        int r = Random.Range(0, items.Length);

        if (r == 0)
        {
            Spawn_A(p, r);
        }
        else
        {
            Spawn_B(p, r);
        }

        // < %50
        int t = Random.Range(0, 101);
        if (t < 30)
        {

        }
        // >

        yield return new WaitForSeconds(rate);

        StartCoroutine("Init_Spawn");
    }


    void Spawn_A (int p, int r)
    {
        if (listA.Count > 0)
        {
            GameObject go = listA[0];
            listA.RemoveAt(0);

            go.transform.position = pointsSpawn[p].position;
            go.GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
            lastItem = go;
        }
        else
        {
            GameObject go = Instantiate(items[r], pointsSpawn[p].position, Quaternion.identity) as GameObject;
            go.transform.SetParent(container);
            go.GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
            lastItem = go;
        }
    }

    void Spawn_B (int p, int r)
    {
        if (listB.Count > 0)
        {
            GameObject go = listB[0];
            listB.RemoveAt(0);

            go.transform.position = pointsSpawn[p].position;
            go.GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
            lastItem = go;
        }
        else
        {
            GameObject go = Instantiate(items[r], pointsSpawn[p].position, Quaternion.identity) as GameObject;
            go.transform.SetParent(container);
            go.GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
            lastItem = go;
        }
    }

    //:::::::::::::::::::::::::::::::
    // Set_Speed
    //:::::::::::::::::::::::::::::::
    void Set_Speed (float value)
    {
        speed = value;
    }

    //:::::::::::::::::::::::::::::::
    // Set_Rate
    //:::::::::::::::::::::::::::::::
    void Set_Rate (float value)
    {
        rate = value;
    }


    void Add_To_ListA (GameObject x_item)
    {
        x_item.GetComponent<Rigidbody2D>().velocity = Vector2.down * 0;
        x_item.transform.position = transform.position;
        listA.Add(x_item);
    }

    void Add_To_ListB (GameObject x_item)
    {
        x_item.GetComponent<Rigidbody2D>().velocity = Vector2.down * 0;
        x_item.transform.position = transform.position;
        listB.Add(x_item);
    }


    IEnumerator Init_Level ()
    {
        speed = 3;
        rate = 1.5f;
        yield return new WaitForSeconds(15);

        print("Level 2");
        speed = 3.5f;
        rate = 1;
        yield return new WaitForSeconds(25);

        print("Level 3");
        speed = 4;
        rate = 0.7f;
        yield return new WaitForSeconds(45);

        print("Level 4");
        speed = 4.5f;
        rate = 0.5f;
        yield return new WaitForSeconds(20);

        SpawnFinish();
    }

    void SpawnFinish ()
    {
        print("Finish");
        StopCoroutine("Init_Spawn");
        lastItem.tag = "ItemFinish";
    }

}
