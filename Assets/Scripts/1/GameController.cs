﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour
{
    public int lives;
    public Text text, liveUI, scoreUI, scoreUI_2;
    public GameObject spawner;
    public GameObject viewGameOver;
    public GameObject viewFinish;

    int score;
    // Use this for initialization
    void Start ()
    {
        liveUI.text = "" + lives;
        StartCoroutine("Init_Game");
    }
	
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
            SceneManager.LoadScene(0);
    }

    void GameOver ()
    {
        spawner.SendMessage("FinishGame");

        for (int i = 0; i < viewGameOver.transform.childCount; i++)
            viewGameOver.transform.GetChild(i).gameObject.SetActive(true);

        scoreUI.text = "" + score;
        // Pantalla de GameOver
        // Guardar el Score en pref comprobando si es mayor al score ya guardado.
        // Opcion de Reset Game

    }

    void Finish_Level ()
    {

        for (int i = 0; i < viewFinish.transform.childCount; i++)
            viewFinish.transform.GetChild(i).gameObject.SetActive(true);

        scoreUI_2.text = "" + score;
    }

    public void ResetGame ()
    {
        // Relanzar la Escena
        SceneManager.LoadScene(1);
    }

    void Error ()
    {
        if (lives > 0)
        {
            lives--;
            liveUI.text = "" + lives;
        }
        else
            GameOver();
    }

    IEnumerator Init_Game ()
    {
        text.text = "3";
        yield return new WaitForSeconds(1);
        text.text = "2";
        yield return new WaitForSeconds(1);
        text.text = "1";
        yield return new WaitForSeconds(1);
        text.text = "Start!";

        Color c = text.color;

        for (int i = 0; i < 10; i++)
        {
            c.a = c.a - 0.1f;
            text.color = c;

            yield return new WaitForSeconds(0.05f);
        }

        text.gameObject.SetActive(false);

        spawner.SendMessage("StartGame");
    }

    void addScore (int n)
    {
        score += n;
    }

}
