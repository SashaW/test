﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Touch_Control : MonoBehaviour
{
    public GameObject gif;
    public float sensitivity = 0.025F;
    public Text uiSensitivity;

    bool options = false;
    public bool sine;

    float tiempo;
    int count;

    //:::::::::::::::::::::::::::::::
    // Start
    //:::::::::::::::::::::::::::::::
    void Start ()
    {
        float sensitivityPref = PlayerPrefs.GetFloat("sensitivity");

        if (sensitivityPref <= 0)
        {
            PlayerPrefs.SetFloat("sensitivity", sensitivity);

            print("Prefs Set !! | " + sensitivityPref);
        }
        else
        {
            sensitivity = sensitivityPref;
            Update_UI();
        }
    }

    //:::::::::::::::::::::::::::::::
    // Update
    //:::::::::::::::::::::::::::::::
    void Update ()
    {
        Tap();

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            transform.Translate(touchDeltaPosition.x * sensitivity, touchDeltaPosition.y * sensitivity, 0);
        }

        if (sine)
            Sine();
    }

    //:::::::::::::::::::::::::::::::
    // Sensitivity_Down
    //:::::::::::::::::::::::::::::::
    public void Sensitivity_Down ()
    {
        if (sensitivity > 0.01f)
            sensitivity -= 0.005f;

        print("Down Sensitivity = " + sensitivity + " | " + sensitivity * 100);

        Update_UI();
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
    }

    //:::::::::::::::::::::::::::::::
    // Sensitivity_Up
    //:::::::::::::::::::::::::::::::
    public void Sensitivity_Up ()
    {
        if (sensitivity < 0.05f)
            sensitivity += 0.005f;

        print("Up Sensitivity = " + sensitivity + " | " + sensitivity * 100);

        Update_UI();
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
    }

    //:::::::::::::::::::::::::::::::
    // ShowOptions
    //:::::::::::::::::::::::::::::::
    public void ShowOptions ()
    {
        GameObject go = GameObject.FindGameObjectWithTag("options");

        for (int i = 0; i < go.transform.childCount; i++)
            go.transform.GetChild(i).gameObject.SetActive(true);
        Update_UI();
    }

    //:::::::::::::::::::::::::::::::
    // HideOptions
    //:::::::::::::::::::::::::::::::
    public void HideOptions ()
    {
        GameObject go = GameObject.FindGameObjectWithTag("options");

        for (int i = 0; i < go.transform.childCount; i++)
            go.transform.GetChild(i).gameObject.SetActive(false);
    }

    //:::::::::::::::::::::::::::::::
    // Options
    //:::::::::::::::::::::::::::::::
    public void Options ()
    {
        print("Options " + options);

        if (options)
            HideOptions();
        else
            ShowOptions();

        options = !options;

    }

    //:::::::::::::::::::::::::::::::
    // Update_UI
    //:::::::::::::::::::::::::::::::
    void Update_UI ()
    {
        if (options)
            uiSensitivity.text = "" + sensitivity * 100;
    }

    //:::::::::::::::::::::::::::::::
    // Sine
    //:::::::::::::::::::::::::::::::
    void Sine ()
    {
        Transform son = transform.GetChild(0).transform;
        Vector2 p = son.localPosition;
        float y = Mathf.Sin(Time.time * 1);

        y = Map(y, 0, 1, 0, 0.15f);

        p.y = y;
        son.localPosition = p;
    }

    //:::::::::::::::::::::::::::::::
    // Map
    //:::::::::::::::::::::::::::::::
    float Map (float v, float in_min, float in_max, float out_min, float out_max)
    {
        return (v - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    //:::::::::::::::::::::::::::::::
    // Tap
    //:::::::::::::::::::::::::::::::
    void Tap ()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            // Handle finger movements based on touch phase.
            switch (touch.phase)
            {
            // Record initial touch position.
                case TouchPhase.Began:
                    tiempo = Time.time;
                    break;

            // Determine direction by comparing the current touch position with the initial one.
                case TouchPhase.Moved:
                    break;

            // Report that a direction has been chosen when the finger is lifted.
                case TouchPhase.Ended:
                    if (Time.time - tiempo < 0.2f)
                    {
                        count++;
                        print("Tap" + count + "| r : " + (tiempo - Time.time));
                        Instantiate(gif, transform.position, Quaternion.identity);
                    }
                    break;
            }
        }
    }
}
