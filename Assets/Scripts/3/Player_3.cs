﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Player_3 : MonoBehaviour
{
    public float sensitivity = 0.025F;
    public Text uiSensitivity;
    public Text uiScore;
    public Text uiScoreFinal;
    public Text uiLives;
    public int lives;

    public GameObject viewGameOver;

    bool options = false;
    int count;

    //:::::::::::::::::::::::::::::::
    // Start
    //:::::::::::::::::::::::::::::::
    void Start ()
    {
        uiLives.text = lives.ToString();
    }

    //:::::::::::::::::::::::::::::::
    // Update
    //:::::::::::::::::::::::::::::::
    void Update ()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0);
        
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            transform.Translate(touchDeltaPosition.x * sensitivity, touchDeltaPosition.y * sensitivity, 0);
        }
    }

    //:::::::::::::::::::::::::::::::
    // OnTriggerEnter2D
    //:::::::::::::::::::::::::::::::
    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "Gif")
        {
            //print("Gif !!");
            count++;
            uiScore.text = "" + count;
            //
            other.GetComponent<Rigidbody2D>().velocity = Vector2.down * 0;
            GameObject.FindWithTag("Spawner").SendMessage("AddGif", other.gameObject);
            //Destroy(other.gameObject, 0.1f);
        }
        else if (other.gameObject.tag == "Obstacle")
        {
            //
            if (lives > 0)
            {
                lives--;
                uiLives.text = lives.ToString();
            }
            else
            {
                //Game Over
                gameObject.GetComponent<CircleCollider2D>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;

                //GameObject.FindWithTag("Spawner").SendMessage("StopGame");

                for (int i = 0; i < viewGameOver.transform.childCount; i++)
                    viewGameOver.transform.GetChild(i).gameObject.SetActive(true);

                uiScoreFinal.text = count.ToString();
            }
            //print("Hit !!");
            //other.GetComponent<CircleCollider2D>().enabled = false;

            //
            other.GetComponent<Rigidbody2D>().velocity = Vector2.down * 0;
            GameObject.FindWithTag("Spawner").SendMessage("AddObstacle", other.gameObject);
            //Destroy(other.gameObject);
        }
    }

    //:::::::::::::::::::::::::::::::
    // OnCollisionEnter2D
    //:::::::::::::::::::::::::::::::
    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.tag == "Gif")
        {
            print("Gif !! - 2");
            //count++;
        }

    }

    void Set_Sensitivity (float value)
    {
        sensitivity = value;
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(2);
    }


}
