﻿using UnityEngine;
using System.Collections;

public class Cleanner : MonoBehaviour
{

    // Use this for initialization
    void Start ()
    {
	
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }
    
    // Bitbucked
    //:::::::::::::::::::::::::::::::
    // OnTriggerEnter2D
    //:::::::::::::::::::::::::::::::
    void OnTriggerEnter2D (Collider2D other)
    {
        other.GetComponent<Rigidbody2D>().velocity = Vector2.down * 0;

        if (other.gameObject.tag == "Gif")
        {
            GameObject.FindWithTag("Spawner").SendMessage("AddGif", other.gameObject);
            //Destroy(other.gameObject, 0.1f);
        }

        else if (other.gameObject.tag == "Obstacle")
        {
            GameObject.FindWithTag("Spawner").SendMessage("AddObstacle", other.gameObject);
            //Destroy(other.gameObject, 0.1f);
        }
    }
}
