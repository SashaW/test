﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Test_Main : MonoBehaviour {

	// Use this for initialization
	public void Scene1 () {
        print("Tap");
        SceneManager.LoadScene(1);
	}
	
	// Update is called once per frame
    public void Scene2 () {
        SceneManager.LoadScene(2);
	}

    public void Scene3 () {
        SceneManager.LoadScene(3);
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
            Application.Quit();
    }
}
