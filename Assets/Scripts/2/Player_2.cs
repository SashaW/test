﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Player_2 : MonoBehaviour
{
    public float sensitivity = 0.025F;
    public Text uiSensitivity;
    public Text uiScore;
    public Text uiScoreFinal;
    public Text uiLives;
    public int lives;

    public GameObject viewGameOver;

    public Transform pointShoot;
    public GameObject shot;
    public float speedShot;

    bool options = false;
    int count;

    public List<GameObject> list_Shots = new List<GameObject>();

    //:::::::::::::::::::::::::::::::
    // Start
    //:::::::::::::::::::::::::::::::
    void Start ()
    {
        uiLives.text = lives.ToString();
    }

    //:::::::::::::::::::::::::::::::
    // Update
    //:::::::::::::::::::::::::::::::
    void Update ()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0);
        
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            transform.Translate(touchDeltaPosition.x * sensitivity, touchDeltaPosition.y * sensitivity, 0);
        }
    }

    //:::::::::::::::::::::::::::::::
    // OnTriggerEnter2D
    //:::::::::::::::::::::::::::::::
    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "Gif")
        {
            //print("Gif !!");
            count++;
            uiScore.text = "" + count;
            //
            other.GetComponent<Rigidbody2D>().velocity = Vector2.down * 0;
            GameObject.FindWithTag("Spawner").SendMessage("AddGif", other.gameObject);
            //Destroy(other.gameObject, 0.1f);
        }
        else if (other.gameObject.tag == "Obstacle")
        {
            //
            if (lives > 0)
            {
                lives--;
                uiLives.text = lives.ToString();
            }
            else
            {
                //Game Over
                gameObject.GetComponent<CircleCollider2D>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;

                //GameObject.FindWithTag("Spawner").SendMessage("StopGame");

                for (int i = 0; i < viewGameOver.transform.childCount; i++)
                    viewGameOver.transform.GetChild(i).gameObject.SetActive(true);

                uiScoreFinal.text = count.ToString();
            }
            //print("Hit !!");
            //other.GetComponent<CircleCollider2D>().enabled = false;

            //
            other.GetComponent<Rigidbody2D>().velocity = Vector2.down * 0;
            GameObject.FindWithTag("Spawner").SendMessage("AddObstacle", other.gameObject);
            //Destroy(other.gameObject);
        }
    }

    //:::::::::::::::::::::::::::::::
    // OnCollisionEnter2D
    //:::::::::::::::::::::::::::::::
    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.tag == "Gif")
        {
            print("Gif !! - 2");
            //count++;
        }

    }

    void Set_Sensitivity (float value)
    {
        sensitivity = value;
    }

    public void ResetGame ()
    {
        SceneManager.LoadScene(2);
    }


    void Shoot ()
    {
        if (list_Shots.Count > 0)
        {
            GameObject go = list_Shots[0];
            list_Shots.RemoveAt(0);
            go.transform.position = pointShoot.position;
            go.GetComponent<Rigidbody2D>().velocity = Vector2.right * speedShot;
        }
        else
        {
            GameObject go = Instantiate(shot, pointShoot.position, Quaternion.identity) as GameObject;
            //go.transform.SetParent(container);
            go.transform.name = "Shot";
            go.GetComponent<Rigidbody2D>().velocity = Vector2.right * speedShot;
        }
    }

    void Add_Shot (GameObject x_shot)
    {
        list_Shots.Add(x_shot);
    }

}
