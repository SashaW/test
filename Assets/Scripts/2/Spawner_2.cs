﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner_2 : MonoBehaviour
{
    public Transform container;

    public GameObject gif;
    public GameObject[] obstacles;
    public Transform pointsG;
    public Transform pointsO;

    public Transform[] pointsGif;
    public Transform[] pointsObstacles;

    public float speed_Gifs = 2;
    public float speed_Obstacles = 4;
    public float rate_Gifs = 2;
    public float rate_Obstacles = 3;

    // TEST
    public List<GameObject> listGifs = new List<GameObject>();
    public List<GameObject> listObtacles = new List<GameObject>();

    int countGifs;

    //:::::::::::::::::::::::::::::::
    // Start
    //:::::::::::::::::::::::::::::::
    void Start ()
    {
        pointsGif = pointsG.GetComponentsInChildren<Transform>();
        pointsObstacles = pointsO.GetComponentsInChildren<Transform>();

        StartCoroutine("Init_Level");
        StartCoroutine("Spawn_Gif");
        StartCoroutine("Spawn_Obstacles");
    }
	
    //:::::::::::::::::::::::::::::::
    // Update
    //:::::::::::::::::::::::::::::::
    void Update ()
    {
	    
    }

    //:::::::::::::::::::::::::::::::
    // Spawn_Gif
    //:::::::::::::::::::::::::::::::
    IEnumerator Spawn_Gif ()
    {
        Spawn_Gifs_2();
        /*
        int r = Random.Range(0, pointsGif.Length);

        GameObject go = Instantiate(gif, pointsGif[r].position, Quaternion.identity) as GameObject;
        go.GetComponent<Rigidbody2D>().velocity = Vector2.left * speed_Gifs;
        */

        //float r2 = Random.Range(2f, 4f);

        yield return new WaitForSeconds(rate_Gifs);

        StartCoroutine("Spawn_Gif");
    }

    //:::::::::::::::::::::::::::::::
    // Spawn_Obstacles
    //:::::::::::::::::::::::::::::::
    IEnumerator Spawn_Obstacles ()
    {
        Spawn_Obtacles_2();
        /*int r = Random.Range(0, pointsObstacles.Length);
        int r1 = Random.Range(0, obstacles.Length);

        GameObject go = Instantiate(obstacles[r1], pointsObstacles[r].position, Quaternion.identity) as GameObject;
        go.transform.eulerAngles = new Vector3(0, 0, 225);
        go.GetComponent<Rigidbody2D>().velocity = go.transform.right * speed_Obstacles;*/

        //float r2 = Random.Range(2f, 4f);

        yield return new WaitForSeconds(rate_Obstacles);

        StartCoroutine("Spawn_Obstacles");
    }

    //:::::::::::::::::::::::::::::::
    // Testing Options  <Call with SendMessage>
    //:::::::::::::::::::::::::::::::

    void Set_Speed_Gifs (float value)
    {
        speed_Gifs = value;
    }

    void Set_Rate_Gifs (float value)
    {
        rate_Gifs = value;
    }

    void Set_Speed_Obstacles (float value)
    {
        speed_Obstacles = value;
    }

    void Set_Rate_Obstacles (float value)
    {
        rate_Obstacles = value;
    }

    //:::::::::::::::::::::::::::::::
    // Testing
    //:::::::::::::::::::::::::::::::
    void Init ()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject go = Instantiate(obstacles[0], transform.position, Quaternion.identity) as GameObject;
            listGifs.Add(go);
        }
    }

    void Spawn_Obtacles_2 ()
    {
        int r = Random.Range(0, pointsObstacles.Length);

        if (listObtacles.Count > 0)
        {
            GameObject go = listObtacles[listObtacles.Count - 1];
            listObtacles.Remove(go);

            go.transform.position = pointsObstacles[r].position;
            go.transform.eulerAngles = new Vector3(0, 0, 225);
            go.GetComponent<Rigidbody2D>().velocity = go.transform.right * speed_Obstacles;
        }
        else
        {
            int r1 = Random.Range(0, obstacles.Length);
            GameObject go = Instantiate(obstacles[r1], pointsObstacles[r].position, Quaternion.identity) as GameObject;
            go.transform.SetParent(container);
            go.transform.name = "Obstacle";
            go.transform.eulerAngles = new Vector3(0, 0, 225);
            go.GetComponent<Rigidbody2D>().velocity = go.transform.right * speed_Obstacles;
        }
    }

    void Spawn_Gifs_2()
    {
        int r = Random.Range(0, pointsGif.Length);

        if (listGifs.Count > 0)
        {
            GameObject go = listGifs[0];
            listGifs.RemoveAt(0);

            go.transform.position = pointsGif[r].position;
            go.GetComponent<Rigidbody2D>().velocity = Vector2.left * speed_Gifs;
        }
        else
        {
            countGifs++;
            GameObject go = Instantiate(gif, pointsGif[r].position, Quaternion.identity) as GameObject;
            go.transform.SetParent(container);
            go.transform.name = "Gif_" + countGifs;
            go.GetComponent<Rigidbody2D>().velocity = Vector2.left * speed_Gifs;
        }
    }

    void AddObstacle (GameObject go)
    {
        go.transform.position = transform.position;
        listObtacles.Add(go);
    }

    void AddGif (GameObject go)
    {
        go.transform.position = transform.position;
        listGifs.Add(go);
    }

    void StopGame()
    {
        StopCoroutine("Spawn_Gif");
        StopCoroutine("Spawn_Obstacles");
    }


    IEnumerator Init_Level ()
    {
        speed_Gifs = 2;
        rate_Gifs = 1.5f;
        speed_Obstacles = 3;
        rate_Obstacles = 0.7f;
        yield return new WaitForSeconds(30);

        print("Level 2");
        speed_Gifs = 3.5f;
        rate_Gifs = 1;
        speed_Obstacles = 5.5f;
        rate_Obstacles = 0.5f;
        yield return new WaitForSeconds(100);

        print("Level 3");
        speed_Gifs = 5;
        rate_Gifs = 1;
        speed_Obstacles = 7;
        rate_Obstacles = 0.2f;
        yield return new WaitForSeconds(120);

        print("Level 4");
        speed_Gifs = 6.5f;
        rate_Gifs = 1;
        speed_Obstacles = 6.5f;
        rate_Obstacles = 0.1f;
        yield return new WaitForSeconds(70);

        print("Finish");
    }


    void Boss ()
    {
        StopGame();
        // invocar Boss
        // 
    }
}
