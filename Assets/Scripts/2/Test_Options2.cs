﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Test_Options2 : MonoBehaviour
{
    public float sensitivity = 0.025F;
    public Text _uiSensitivity, _uiSpeedGifs, _uiRateGifs, _uiSpeedObstacles, _uiRateObstacles;

    public float unit = 0.1f;


    public float speed_Gifs;
    public float speed_Obstacles;
    public float rate_Gifs;
    public float rate_Obstacles;

    bool options = false;

    // Use this for initialization
    void Awake ()
    {
        string prefs = PlayerPrefs.GetString("Player3");

        if (prefs == "Player3")
        {
            print ("Prefs Player3 : " + prefs);
            Load_Prefs();
            Set_All ();
        } else {
            print ("Prefs Player3 : " + prefs);
            Save_Prefs();
            PlayerPrefs.SetString("Player3", "Player3");
        }
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }

    //:::::::::::::::::::::::::::::::
    // ShowOptions
    //:::::::::::::::::::::::::::::::
    public void ShowOptions ()
    {
        Time.timeScale = 0;
        GameObject go = GameObject.FindGameObjectWithTag("options");

        for (int i = 0; i < go.transform.childCount; i++)
            go.transform.GetChild(i).gameObject.SetActive(true);
        
        Update_UI();
    }

    //:::::::::::::::::::::::::::::::
    // HideOptions
    //:::::::::::::::::::::::::::::::
    public void HideOptions ()
    {
        Time.timeScale = 1;
        GameObject go = GameObject.FindGameObjectWithTag("options");

        for (int i = 0; i < go.transform.childCount; i++)
            go.transform.GetChild(i).gameObject.SetActive(false);
    }

    //:::::::::::::::::::::::::::::::
    // Options
    //:::::::::::::::::::::::::::::::
    public void Options ()
    {
        print("Options " + options);

        if (options)
            HideOptions();
        else
            ShowOptions();

        options = !options;

    }

    //:::::::::::::::::::::::::::::::
    // Update_UI
    //:::::::::::::::::::::::::::::::
    void Update_UI ()
    {
        _uiSensitivity.text = (sensitivity * 10f).ToString("0.00");
        _uiSpeedGifs.text = speed_Gifs.ToString("0.0");
        _uiRateGifs.text = rate_Gifs.ToString("0.0");
        _uiSpeedObstacles.text = speed_Obstacles.ToString("0.0");
        _uiRateObstacles.text = rate_Obstacles.ToString("0.0");

        Set_All();
    }

    //:::::::::::::::::::::::::::::::
    // Testing Options
    //:::::::::::::::::::::::::::::::
    public void _Rate_Down_Gifs ()
    {
        rate_Gifs -= unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Rate_Up_Gifs ()
    {
        rate_Gifs += unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Speed_Down_Gifs ()
    {
        speed_Gifs -= unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Speed_Up_Gifs ()
    {
        speed_Gifs += unit;
        Update_UI();
        Save_Prefs();
    }


    //:::::::::::::::::::::::::::::::
    // Obstacles 
    //:::::::::::::::::::::::::::::::

    public void _Rate_Down_Obstacles ()
    {
        rate_Obstacles -= unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Rate_Up_Obstacles ()
    {
        rate_Obstacles += unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Speed_Down_Obstacles ()
    {
        speed_Obstacles -= unit;
        Update_UI();
        Save_Prefs();
    }

    public void _Speed_Up_Obstacles ()
    {
        speed_Obstacles += unit;
        Update_UI();
        Save_Prefs();
    }

    //:::::::::::::::::::::::::::::::
    // Load and Save 
    //:::::::::::::::::::::::::::::::
    void Load_Prefs ()
    {
        speed_Gifs = PlayerPrefs.GetFloat("speed_Gifs");
        rate_Gifs = PlayerPrefs.GetFloat("rate_Gifs");
        speed_Obstacles = PlayerPrefs.GetFloat("speed_Obstacles");
        rate_Obstacles = PlayerPrefs.GetFloat("rate_Obstacles");
        sensitivity = PlayerPrefs.GetFloat("sensitivity");
    }

    void Save_Prefs ()
    {
        PlayerPrefs.SetFloat("speed_Gifs", speed_Gifs);
        PlayerPrefs.SetFloat("rate_Gifs", rate_Gifs);
        PlayerPrefs.SetFloat("speed_Obstacles", speed_Obstacles);
        PlayerPrefs.SetFloat("rate_Obstacles", rate_Obstacles);
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
    }

    void Set_All ()
    {
        GameObject spawner = GameObject.FindWithTag("Spawner");

        spawner.SendMessage("Set_Speed_Gifs", speed_Gifs);
        spawner.SendMessage("Set_Rate_Gifs", rate_Gifs);
        spawner.SendMessage("Set_Speed_Obstacles", speed_Obstacles);
        spawner.SendMessage("Set_Rate_Obstacles", rate_Obstacles);

        GameObject.FindWithTag("Player").SendMessage("Set_Sensitivity", sensitivity);
    }

    //:::::::::::::::::::::::::::::::
    // Sensitivity_Down
    //:::::::::::::::::::::::::::::::
    public void Sensitivity_Down ()
    {
        if (sensitivity > 0.01f)
            sensitivity -= 0.005f;

        print("Down Sensitivity = " + sensitivity + " | " + sensitivity * 100);

        Update_UI();
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
    }

    //:::::::::::::::::::::::::::::::
    // Sensitivity_Up
    //:::::::::::::::::::::::::::::::
    public void Sensitivity_Up ()
    {
        if (sensitivity < 0.05f)
            sensitivity += 0.005f;

        print("Up Sensitivity = " + sensitivity + " | " + sensitivity * 100);

        Update_UI();
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
    }
}
