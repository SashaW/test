﻿using UnityEngine;
using System.Collections;

public class ChimneyMotion : MonoBehaviour
{
    public float rangeA, rangeB;
    public float PositionStart = 16.0f;
    public float PositionFinal = -16.0f;
    float TimeO = 0.0f;

    public float VariatorSpeed = 0.5f;
    float RandomSize;

    void Awake()
    {
        RandomSize = Random.Range(rangeA, rangeB);
    }

	// Use this for initialization
	void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = new Vector3(Mathf.Lerp(PositionStart, PositionFinal, TimeO), RandomSize, 0.0f);

        TimeO += VariatorSpeed * Time.deltaTime;


        if(transform.position.x <= PositionFinal)
            Destroy(gameObject);


	
	}
}
