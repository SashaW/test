﻿using UnityEngine;
using System.Collections;

public class ChimneySpawner : MonoBehaviour
{
    public GameObject m_Chimney;
    //public GameObject m_Spawner;

    GameObject m_m_Chimney;

    // Use this for initialization
    void Start ()
    {
        InvokeRepeating("Spawner", 1.0f, 2.0f);
        //m_Chimney = (GameObject)Resources.Load("Chimney");
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void Spawner()
    {
        
        Instantiate(m_Chimney, transform.position, transform.rotation);
        
    }
}
