﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Tap_Test : MonoBehaviour
{
    public bool init = false;
    public Text tapsUI, timeUI;
    public int tapsMax;

    public int count;
    public Transform posShots;

    int taps;
    bool active = true;

    // optionals
    public float time;
    public int rate;
    public Slider bar;

    //:::::::::::::::::::::::::::::::
    // Start
    //:::::::::::::::::::::::::::::::
    void Start ()
    {
        timeUI.text = count.ToString();
    }
	
    //:::::::::::::::::::::::::::::::
    // Update
    //:::::::::::::::::::::::::::::::
    void Update ()
    {
        if (active)
            Tap();

        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0);
    }

    // Line 1
    // Test Sasha
    
    //:::::::::::::::::::::::::::::::
    // Tap
    //:::::::::::::::::::::::::::::::
    void Tap ()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            // Handle finger movements based on touch phase.
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (!init)
                    {
                        StartCoroutine("Cronometro");
                        init = !init;
                    }
                    /*if (!init)
                    {
                        StartCoroutine("Init_Taps");
                        init = !init;
                    }*/

                    Vector2 pos = new Vector2(Camera.main.ScreenToWorldPoint(touch.position).x, Camera.main.ScreenToWorldPoint(touch.position).y);
                    Collider2D other = Physics2D.OverlapPoint(pos);

                    if (other != null && other.tag == "Boss")
                    {
                        if (taps == (tapsMax - 1))
                        {
                            // Win Game
                            print("Win Game");
                            active = false;
                            StopCoroutine("Cronometro");
                        }

                        GameObject.FindWithTag("Player").SendMessage("Shoot");
                        taps++;
                        tapsUI.text = taps + " / " + tapsMax;
                        //Update_Slice();
                        //print("Tap :  " + _taps);
                    }
                    else
                        print("Touch Error !");
                    break;
            }
        }
    }

    //:::::::::::::::::::::::::::::::
    // Init_Taps
    //:::::::::::::::::::::::::::::::
    IEnumerator Init_Taps ()
    {
        count += rate;
        tapsUI.text = taps + " / " + count;
        Update_Slice();
        yield return new WaitForSeconds(time);
        StartCoroutine("Init_Taps");
    }

    //:::::::::::::::::::::::::::::::
    // Cronometro
    //:::::::::::::::::::::::::::::::
    IEnumerator Cronometro ()
    {        
        yield return new WaitForSeconds(1);

        if (count > 0)
        {
            count--;
            StartCoroutine("Cronometro");
        }
        else
        {
            print("Finish Time");
            active = false;
            if (taps < tapsMax)
            {
                // Win Game
                print("Game Over");
            }
        }

        timeUI.text = count.ToString();
    }

    //:::::::::::::::::::::::::::::::
    // Update_Slice
    //:::::::::::::::::::::::::::::::
    void Update_Slice ()
    {        
        float v = Map(taps, 0, count, 0, 1);
        bar.value = v;
    }

    //:::::::::::::::::::::::::::::::
    // Map
    //:::::::::::::::::::::::::::::::
    float Map (float v, float in_min, float in_max, float out_min, float out_max)
    {
        return (v - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    //:::::::::::::::::::::::::::::::
    // OnTriggerEnter2D
    //:::::::::::::::::::::::::::::::
    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "Box")
        {
            other.transform.position = posShots.position;
            other.GetComponent<Rigidbody2D>().velocity = Vector2.right * 0;
            GameObject.FindWithTag("Player").SendMessage("Add_Shot", other.gameObject);
        }

    }
}
